﻿using UnityEngine;
using System.Collections;

public class ConstructBoard : MonoBehaviour {

	public GameObject block;

	public int rows = 3;
	public int cols = 3;

	public float marginRow = 1.0f;
	public float marginCol = 1.0f;

	private Vector3 [][] positionMatrix;//matriz de posiciones
	private GameObject [][] blockMatrix;//matriz de gameobjects

	void Start () {

		generarPosiciones ();
		generarBloques ();			
	}

	void generarPosiciones (){//genera matrices y posiciones

		Vector3 positionBlock = Vector3.zero;

		//inicializamos y construimos las matrices
		positionMatrix = new Vector3[rows][];
		blockMatrix = new GameObject[rows][];

		//desplazamiento hz
		float xOffSet = positionMatrix.Length/rows;

		for (int i = 0; i<rows; i++) {
			positionMatrix [i] = new Vector3[cols];
			blockMatrix [i] = new GameObject[cols];

			float yOffSet = positionMatrix [i].Length/cols;//desplazamiento vert

			positionBlock.z = (rows-i-xOffSet-1)*marginRow;

			for(int j=0; j<cols; j++){
				positionBlock.x = (j-yOffSet)*marginCol;
				positionMatrix[i][j] = positionBlock;
			}
		}
	}

	void generarBloques(){

		int posicion = 0;

		for (int i = 0; i<rows; i++) {
			for(int j=0; j<cols; j++){
				GameObject bloqueActual = Instantiate (block, positionMatrix[i][j], Quaternion.identity) as GameObject;

				bloqueActual.SendMessage("setRow", i);
				bloqueActual.SendMessage("setCol", j);
				bloqueActual.SendMessage("setPosicion", posicion);
				posicion++;

				blockMatrix[i][j] = bloqueActual;
			}
		}

		SendMessage ("setBlockMatrix", blockMatrix);//Enviamos blockMatrix a manager para que controle la partida (con poner sendmenssage vale xq los scripts estan dentro del mismo g.o.)
	}
}