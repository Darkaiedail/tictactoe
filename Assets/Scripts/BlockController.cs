﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour {

	private int state = 0;//me controla si tengo una pieza encima de la base o no

	public int row;
	public int col;

	private int posicion;//con posicion nos referimos al codigo de cada bloque (0 1 2 3..)

	private GameObject managerGObj;//variable para guardar el go
	private Manager managerComp;//variable para guardar el comp

	//metodos de acceso a sus propiedades o variables
	void setRow (int row){
		this.row = row;
	}

	void setCol (int col){
		this.col = col;
	}

	void setState(int state){
		this.state = state;
	}

	void setPosicion (int posicion){
		this.posicion = posicion;
	}

	void Start () {
		managerGObj = GameObject.FindGameObjectWithTag ("Manager");	
		managerComp = managerGObj.GetComponent<Manager>();//para extraer el componente del gameobj managerGObj
	}

	void OnMouseDown (){

		if (state == 0) {//estado 0 el bloque esta vacio
			managerComp.clickOnBlock (this.gameObject, row, col, posicion);//llamo a la funcion que e creado en manager por lo que ya puedo meter todos los parametros que necesito saber
		}
	}
}
