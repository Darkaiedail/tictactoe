﻿using UnityEngine;
using System.Collections;
using System.Text;//coleccion de objetos utiles para manejar cadenas de texto (stringBuilder)

public class Manager : MonoBehaviour {

	private GameObject [][] blockMatrix;//matriz de gameobjects

	private int[][] board;

	public GameObject piezaPlayer1;
	public GameObject piezaPlayer2;

	private int turno = 1;

	private int stateGame = 0;//0 es playing, 1 win payer one, 2 win player 2, 3 empate

	//clips de audio
	public AudioSource xwins;
	public AudioSource owins;
	public AudioSource draw;
	public AudioSource button;
	public AudioSource letsPlay;

	//seleccion 1 o 2 player
	public bool ia = true;

	void Start(){

		//iniciamos matriz board (sirve para encontrar triadas)
		board = new int[3][];
		letsPlay.Play ();

		for (int i =0; i<3; i++) {//filas
			board[i] = new int[3];

			for(int j=0; j<3; j++){//columnas
				board [i][j] = 0;
			}
		}
	}

	void OnGUI(){
		/*StringBuilder builder = new StringBuilder ();

		for (int i =0; i<3; i++) {//filas
			for(int j=0; j<3; j++){//columnas
				int codigo = board [i][j];
				builder.Append(codigo);
			}
			builder.AppendLine();//despues de cada fila me agrega un /n o sea un salto de fila por eso lo pongo justo cuando acaba el segundo bucle for
		}
		GUI.TextField (new Rect (10, 10, 30, 60), builder.ToString ());*///despues del .tostring pongo parentesis xq builder es un array dinamico
	}

	//Call from ConstructBoard
	void setBlockMatrix(GameObject [][] blockMatrix){
		this.blockMatrix = blockMatrix;
	}

	//es llamada desde cada bloque en el momento de acer click
	public void clickOnBlock(GameObject sender, int row, int col, int posicion){

		if (stateGame == 0) {//con este if lo q ace es que solo funciona si estas en estado 0 por lo que al cambiar el estado cuando gana o empata cambia el estado a 1 2 3 por lo que no continua
			//print (posicion);
			Vector3 posicionPieza = blockMatrix [row] [col].transform.position;

			posicionPieza.y = 0.5f;//correccion de Y

			float randomRotation = Random.Range (60.0f, 130.0f);
			Quaternion rotacionPieza = Quaternion.Euler (randomRotation, randomRotation, randomRotation);

			if (turno == 1) {//juega player 1
				board [row] [col] = 1;
				sender.SendMessage ("setState", turno);
				button.Play();

				Instantiate (piezaPlayer1, posicionPieza, rotacionPieza);

				gameState (board, 3);//ejecuta la funcion que me comprueba el estado de la partida

				turno = 2;//pasamos el turno a player 2

				if (ia == true && stateGame == 0){
					iaEasyDecision();
					gameState(board, 3);
					turno = 1;
				}
			} else if (turno == 2) {//juega player 2 
					
				if (ia == false){
					board [row] [col] = 2;
					sender.SendMessage ("setState", turno);
					button.Play();

					Instantiate (piezaPlayer2, posicionPieza, rotacionPieza);

					gameState (board, 3);//ejecuta la funcion que me comprueba el estado de la partida

					turno = 1;	//pasamos el turno a player 1	
				}
			}
		}
	}

	void iaEasyDecision(){

		//Try center
		GameObject selectIABlock = takeCenter ();

		if (selectIABlock != null) {//centro esta libre
			iaDrop (selectIABlock);
		} else {//el centro esta ocupado
			selectIABlock = takeAnyCorner();
			if (selectIABlock != null) {//corner esta libre
				iaDrop (selectIABlock);
			}else{//las esquinas estan ocupadas
				selectIABlock = takeAnyDirection();
				if (selectIABlock != null) {//cruceta esta libre
					iaDrop (selectIABlock);
				}
			}
		}

	}

	//funcion con la q la ia comprueba si el centro esta ocupado
	GameObject takeCenter(){

		if (board [1] [1] == 0) {
			return blockMatrix [1][1];
		}
		return null;
	}

	//funcion cn la q la ia comprueba si las esquinas estan ocupadas
	GameObject takeAnyCorner(){
	
		if (board [0][0] == 0) {//Top-left
			return blockMatrix [0][0];
		}else if (board [0][2] == 0) {//Top-right
			return blockMatrix [0][2];
		}else if (board [2][0] == 0) {//bottom-left
			return blockMatrix [2][0];
		}else if (board [2][2] == 0) {//bottom-right
			return blockMatrix [2][2];
		}
		return null;
	}

	//funcion cn la q la ia comprueba si la cruceta esta ocupada
	GameObject takeAnyDirection(){

		if (board [0][1] == 0) {//Top
			return blockMatrix [0][1];
		}else if (board [1][2] == 0) {//right
			return blockMatrix [1][2];
		}else if (board [2][1] == 0) {//bottom
			return blockMatrix [2][1];
		}else if (board [1][0] == 0) {//left
			return blockMatrix [1][0];
		}
		return null;
	}


	//funcion para crear IA
	void iaDrop(GameObject selectIABlock){

		BlockController controller = selectIABlock.GetComponent<BlockController> ();

		int row = controller.row;
		int col = controller.col;

		Vector3 posicionPieza = blockMatrix [row] [col].transform.position;
		
		posicionPieza.y = 0.5f;//correccion de Y
		float randomRotation = Random.Range (60.0f, 130.0f);
		Quaternion rotacionPieza = Quaternion.Euler (randomRotation, randomRotation, randomRotation);
	
		board [row] [col] = 2;
		selectIABlock.SendMessage ("setState", turno);
		
		Instantiate (piezaPlayer2, posicionPieza, rotacionPieza);
		
		gameState (board, 3);//ejecuta la funcion que me comprueba el estado de la partida

	}

	//Funcion que sirve para cualquier tipo de matriz, me saca las triadas en filas col y diagonales
	void gameState(int[][] board,int boardSz) {		
		
		bool[] colCheckNotRequired = new bool[boardSz];//default is false
		bool diag1CheckNotRequired = false;
		bool diag2CheckNotRequired = false;
		bool allFilled = true;
				
		int player1_count = 0;
		int player2_count = 0;		
		
		//Me comprueba las filas
		for (int i = 0; i < boardSz; i++) {
			
			player1_count = player2_count = 0;
			
			for (int j = 0; j < boardSz; j++) {
				if(board[i][j] == 1){
					player1_count++;
				}
				if(board[i][j] == 2){
					player2_count++;
				}
				if(board[i][j] == 0){
					colCheckNotRequired[j] = true;
					
					if(i==j){ 
						diag1CheckNotRequired = true;
					}
					
					if(i + j == boardSz - 1){
						diag2CheckNotRequired = true;
					}
					
					allFilled = false;
					//No need check further
					break;
				}
			}
			if(player1_count == boardSz){
				print ("Win Player 1");
				xwins.Play();
				stateGame = 1;
				Application.LoadLevel(0);
			}
			if(player2_count == boardSz){
				print ("Win Player 2");
				owins.Play();
				stateGame = 2;
				Application.LoadLevel(0);
			}
		}		
		
		//me comprueba las columnas
		for (int i = 0; i < boardSz; i++) {
			
			player1_count = player2_count = 0;
			
			if(colCheckNotRequired[i] == false){
				
				for (int j = 0; j < boardSz; j++) {
					if(board[j][i] == 1){
						player1_count++;
					}
					if(board[j][i] == 2){
						player2_count++;
					}
					//No need check further
					if(board[i][j] == 0){
						//break;
					}
				}
				if(player1_count == boardSz){
					print ("Win Player 1");
					xwins.Play();
					stateGame = 1;
					Application.LoadLevel(0);
				}
				if(player2_count == boardSz){
					print ("Win Player 2");
					owins.Play();
					stateGame = 2;
					Application.LoadLevel(0);
				}
			}
		}		
		
		player1_count = player2_count = 0;
		//me comprueba la diag de izda a dcha
		if(diag1CheckNotRequired == false){
			
			for (int i = 0; i < boardSz; i++) {
				if(board[i][i] == 1){
					player1_count++;
				}
				if(board[i][i] == 2){
					player2_count++;
				}
				if(board[i][i] == 0){
					break;
				}
			}
			
			if(player1_count == boardSz){
				print ("Win Player 1");
				xwins.Play();
				stateGame = 1;
				Application.LoadLevel(0);
			}
			if(player2_count == boardSz){
				print ("Win Player 2");
				owins.Play();
				stateGame = 2;
				Application.LoadLevel(0);
			}
		}		
		
		player1_count = player2_count = 0;
		//me comprueba la diag de dcha a izda
		if( diag2CheckNotRequired == false){
			for (int i = boardSz - 1,j = 0; i >= 0 && j < boardSz; i--,j++) {
				if(board[j][i] == 1){
					player1_count++;
				}
				if(board[j][i] == 2){
					player2_count++;
				}
				if(board[j][i] == 0){
					break;
				}
			}
			if(player1_count == boardSz){
				print ("Win Player 1");
				xwins.Play();
				stateGame = 1;
				Application.LoadLevel(0);
			}
			if(player2_count == boardSz){
				print ("Win Player 2");
				owins.Play();
				stateGame = 2;
				Application.LoadLevel(0);
			}
			player1_count = player2_count = 0;
		}		

		//es para cuando la partida acaba en empate
		if (stateGame == 0) {
			if (allFilled == true) {
				for (int i = 0; i < boardSz; i++) {
					for (int j = 0; j < boardSz; j++) {
						if (board [i] [j] == 0) {
							allFilled = false;
							break;
						}
					}
					
					if (allFilled == false) {
						break;
					}
				}
			}
			
			if (allFilled) {
				print ("Draw");
				draw.Play ();
				stateGame = 3;
				Application.LoadLevel(0);
			}
		}
	}//Fin gamestate
}